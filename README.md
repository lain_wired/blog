# Lainブログ

私のブログです。

[Hugo](https://gohugo.io/)を用いて作成されております。

## アドレス

https://blog.lain-wired.net/

## License

|適用場所|License|
|---|---|
|Hugoプロジェクトのソースコード|GNU GENERAL PUBLIC LICENSE Version 3|
|ブログ記事|CC BY-NC-SA 4.0|

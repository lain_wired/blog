---
title: 「エンジニアでも恋がしたい!転職初日にぶつかった女の子が同僚だった件」をPython3で解いてみる
author: Lain
type: post
date: 2020-08-03
slug: enkoi-python3
aliases: /archives/237
image: enkoi.png
categories:
  - 技術
tags:
  - Python3

---

高校の情報の期末考査の予習として、[paiza](https://paiza.jp/)というエンジニアの転職･就活･プログラミング学習サイトで昔やっていた[「エンジニアでも恋がしたい!転職初日にぶつかった女の子が同僚だった件」という ~~ありがちな萌え~~ 企画](https://paiza.jp/poh/enkoi/)を解いてみました。

せっかくなので、解答･解説を掲載します。

なお、解答のPython3プログラムは以下のGitLabレポジトリで公開しています。

https://gitlab.com/lain_wired/enkoi

手っ取り早く漫画だけ読みたいという人はコピペしてご利用下さい。

## 第1問

```python:1.py
# -*- coding: utf-8 -*-
num = int(input().rstrip())
result = 0
for i in range(num):
    result += int(input().rstrip())
print(result)
```

標準入力の1行目でデータの個数が渡され、その後1行ずつ渡される整数データの総和を求めるという簡単な問題です。

2行目でデータの個数を取得、3行目でresult変数を初期化し、4･5行目でforループを利用して整数データを1つずつresult変数に加算し、それを6行目で標準出力に出力します。

これはどんな初心者でも解けるでしょう。間違える方が難しいと思います。

## 第2問

```python:2.py
# -*- coding: utf-8 -*-
num = int(input().rstrip())
result = 0
for i in range(num):
    data = [int(i) for i in input().rstrip().split(' ')]
    if data[0] > data[1]:
        result += (data[0] - data[1]) * data[2]
print(result)
```

標準入力の1行目でデータの個数、その後1行ずつスペース区切りの3つの整数データが渡され、その1つ目が2つ目よりも大きいものの(1つ目 &#8211; 2つ目) × 3つ目の総和を求めるという問題です。

2行目でデータの個数を取得、3行目でresult変数を初期化します。  
その後、4~7行目のforループ内で、5行目で3つの整数データを配列として取得、6~7行目のif文でdata[0] > data[1]の場合は(data[0] - data[1]) * data[2]をresult変数に加算し、8行目でresult変数を標準出力に出力します。

しかし、野田さんが使っていたなでしこって、日本語で書けるとしても、brainfuck並みの書きにくさだと思う。

## 第3問

```python:3.py
# -*- coding: utf-8 -*-
meta = [int(i) for i in input().rstrip().split(' ')]
line = []
for i in range(meta[1]):
    line.append(int(input().rstrip()))
max = sum(line[0:meta[0]])
now = max
for i in range(meta[1] - meta[0]):
    now = now + line[i + meta[0]] - line[i]
    if now > max:
        max = now
print(max)
```

標準入力の1行目で「整数n データの個数」が渡され、その後データが1行ずつ渡されます。  
そして、データから連続するn個の整数を切り出したものの総和の最大値を求めるという問題です。

ただし、処理しなければならないデータの個数が多いので、馬鹿正直に処理しているとタイムアウトになってしまいます。

2行目で整数nとデータの個数を取得し、3~5行目でデータを配列に代入します。  
そして、6行目で最初からn個のデータの総和を暫定最大値としてmax変数に代入し、7行目と8~11行目のforループでそこから1つずつ切り出す位置をずらしていき、その部分の総和がmax変数よりも大きかった場合は10･11行目のif文でmax変数にその値を代入します。  
この際、新たな部分の総和を一から計算するのではなく、新たに追加された数字を加算、無くなった部分の数字を減算するという方法を使うことで、処理を高速化しています。  
そして12行目でmax変数を標準出力に出力します。

しかし、このゲーム、絶対につまらないと思う。

## 感想

**漫画のストーリーがベタ過ぎる。**

## 付記

私のPythonのプログラミング環境はJupyter notebook(Debian 10.5.0 buster / aptインストール)です。  
sys.stdin.readlinesを使わないのかという声が上がりそうですが、1行目とそれ以降を分けて処理するのが面倒なので使いませんでした。

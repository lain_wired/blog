---
title: 「CoinIMPで政党を支援しよう」開設のお知らせ
author: Lain
type: post
date: 2019-12-19
slug: release-of-donate-party.org
aliases: /archives/170
image: /image/donate-party.org.png
categories:
  - CoinIMPで政党を支援しよう
  - 活動
tags:
  - Bitcoin
  - CoinIMP
  - CoinIMPで政党を支援しよう
  - Webchain
  - 仮想通貨

---

こんにちは、Lainです。

この度、「CoinIMPで政党を支援しよう」というサイトを開設しました。

  https://donate-party.org/

## 概要

このサイトでは、[CoinIMP](http://www.coinimp.com/invite/01f4c2b4-fb2a-4e41-9bc6-0b8000ae7de1)というサービスを用いて、閲覧者の皆さんに[Webchain](https://webchain.network/)という仮想通貨を採掘してもらいます。

すると、採掘された[Webchain](https://webchain.network/)の99%が私に入りますので、[^1]それを私が現金化し、原則として全額[^2]を、後述のタイミングで政党に寄付します。

また、トップページの広告からの収入を、最も採掘量の多い政党への寄付に上乗せします。

寄付が行われるタイミングは以下の通りです。[^3]

以下の場合は必ず寄付を行います。

  * 衆議院議員総選挙の投票日の12~22日前
  * 参議院議員通常選挙の投票日の17~27日前
  * 憲法改正の国民投票の投票日の20~30日前、50~60日前、80~90日前、110~120日前、140~150日前、170~180日前[^4][^5]

また、以下の場合は、私がその選挙を重要だと考えた場合のみ寄付を行います。

  * 衆議院議員総選挙の投票日の12~22日前 
  * 参議院議員通常選挙の投票日の17~27日前
  * 都道府県知事選挙の投票日の9~19日前
  * 都道府県議会議員選挙の投票日の9~19日前
  * 政令指定都市の市長選挙の投票日の14~24日前
  * 政令指定都市の市議会議員選挙の投票日の9~19日前
  * 政令指定都市以外の市長選挙の投票日の7~17日前
  * 政令指定都市以外の市議会議員選挙の投票日の7~17日前
  * 町村長選挙の投票日の5~15日前
  * 町村議会議員選挙の5~15日前 

その他詳しい寄付の条件は[寄付条件](https://donate-party.org/policy.html)をご覧下さい。

使い方は、トップページに並んでいる政党から自分が寄付したい政党を選んでクリックし、開いたページで広告を閉じた上、「採掘を開始」ボタンを押すだけです。
後は、適当にスレッドとパワー調整を調節して下さい。[^6]

この際、ウイルス対策ソフトなどがCoinIMPのプログラムをマルウェアとして誤検出することがありますが、無視して下さい。

トップページに掲載されているステータスの内

  * レートと採掘量は毎時0分更新
  * 広告料収入は毎日1回 0.001BTC単位で更新

です。

広告出稿の方法は[広告出稿について](/advertising/)をご覧下さい。

  **寄付額は寄付すると決定した時点のレートで決定し、 仮想通貨の価格変動のリスクや取引手数料、ミキシング手数料は私が負担します。**

## お願い

このサイトは、採掘してくれる人の人数に正比例して採掘量が増えます。

そこで

  **このサイトの存在をどんどん拡散して出来るだけ多くの人に採掘してもらって下さい!**


TwitterやFacebookへの投稿でも構いませんし、口コミでも構いません。

ブログやWebサイトでの紹介用に、[バナー](https://donate-party.org/banner.html)も用意してあります。

紹介記事を書いて頂いたら、出来ればTwitterで私に教えてください。(私も読みたいので)

また、このサイトで寄付出来る金額が低いと思うならば、自分のポケットマネーで寄付することも検討して下さい。

## スペシャルサンクス

このサイトの作成に協力して頂いた方々に感謝して、その方々のお名前と公式サイト(またはそれに準ずる物)とTwitterアカウント[^7]を掲載します。
本当にありがとうございました。

### ロゴ作成

  * [きく_いけ_そう](https://note.com/so_harunohi)</a>さん([@so_harunohi](https://twitter.com/so_harunohi))
  * [黒木初](https://kurokihajime.hatenadiary.jp/)さん([@zaregotonandayo](https://twitter.com/zaregotonandayo))

黒木初さんの紹介できく\_いけ\_そうさんにロゴを作成してもらいました。
ありがとうございました。

### デザイン

以下の方々から(現在進行形で)デザインに関して助言を頂いております。
ありがとうございます。

  * [黒木初](https://kurokihajime.hatenadiary.jp/)さん([@zaregotonandayo](https://twitter.com/zaregotonandayo))
  * NAKAMURA Takashiさん([@yokyun68k](https://twitter.com/yokyun68k))
  * 孔悠鬼さん([@kongyouguai](https://twitter.com/kongyouguai))
  * [我流ぱみゅぱみゅ](https://note.com/garyupamyurin)さん([@garyupamyurin](https://twitter.com/garyupamyurin))
  * [きく_いけ_そう](https://note.com/so_harunohi)さん([@so_harunohi](https://twitter.com/so_harunohi))
  * 脱原発.com ＠ 改憲阻止さん([@battlecom](https://twitter.com/battlecom))
  * 在日韓国人への疑問にはお答えしません？さん([@AbeTakakazu_htn](https://twitter.com/AbeTakakazu_htn))

### ライブラリ作成者の方々

ライブラリとは、何らかの機能などを簡単に実装するために、組み込んで使うプログラムのことです。

  * [Bootstrap](https://getbootstrap.com/)の[開発チームの皆さん](https://github.com/orgs/twbs/people)([@getbootstrap](https://twitter.com/getbootstrap))
  * [John Resig](https://johnresig.com/)さん及び[jQuery](https://jquery.com/)開発チームの皆さん([@jeresig](https://twitter.com/jeresig)、[@jquery](https://twitter.com/jquery))
  * [Popper.js](https://popper.js.org/)開発者の[Federico Zivolo](https://fezvrasta.github.io/)さん([@FezVrasta](https://twitter.com/FezVrasta))
  * [CoinIMP](http://www.coinimp.com/invite/01f4c2b4-fb2a-4e41-9bc6-0b8000ae7de1)チーム([@CoinIMP](https://twitter.com/coinimp))
  * [highlight.js](https://highlightjs.org/)の[開発チームの皆さん](https://github.com/highlightjs/highlight.js/blob/master/AUTHORS.en.txt)([@highlightjs](https://twitter.com/highlightjs))
  * [clipboard.js](https://clipboardjs.com/)開発者の[Zeno Rocha](https://zenorocha.com/)さん([@zenorocha](https://twitter.com/zenorocha))
  * Google社の[Google Fonts](https://fonts.google.com/)チームの皆さん([@googlefonts](https://twitter.com/googlefonts))

この方々がいなければ私はこんなサイトを作ることは出来ませんでした。

### サービス提供者の方々

サイトの設置に必要なサービスを提供してくれている方々です。

  * [Cloudflare](https://www.cloudflare.com/)社([@Cloudflare](https://twitter.com/Cloudflare))
  * [Let’s Encrypt](https://letsencrypt.org/)チーム
  * [Anonymous Ads](http://a-ads.com/?partner=1226113)ネットワーク([@aads_network](https://twitter.com/aads_network))
  * [CoinMarketCap](https://coinmarketcap.com/)社([@CoinMarketCap](https://twitter.com/CoinMarketCap))
  * [BTC.com](https://btc.com/)

Cloudflare社にはDDos攻撃対策やサーバーの負荷軽減のためのCDNというサービス及びサーバー証明書を、
Let’s Encryptチームには常時SSL化用のサーバー証明書を、
Anonymous Adsネットワークには匿名広告出稿システムを、
CoinMarketCap社とBTC.comにはステータス表示用のAPIを提供して頂いております。

ありがとうございます。
 
 [^1]: 1%は運営が手数料として持って行きます。
 
 [^2]: 全額だと政治資金規正法に引っかかる場合を除きます。その場合は残金を次回の寄付時に繰り越します。
 
 [^3]: 日付に幅があるのは、この間のいつかに寄付を行うという意味です。
 
 [^4]: ただし、憲法改正国民投票運動期間には幅がありますので、その時点で国民投票が行われると分かっていなかった場合には寄付を行いません。
 
 [^5]: 期間が複数設定されているのは、運動期間が長いからです。
 
 [^6]: 表示されるWebchainの採掘量は手数料を引いた後のものです。
 
 [^7]: それぞれあれば

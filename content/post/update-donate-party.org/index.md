---
title: 「CoinIMPで政党を支援しよう」のURL変更のお知らせ
author: Lain
type: post
date: 2021-04-27
slug: update-donate-party.org
image: /image/donate-party.org.png
categories:
  - CoinIMPで政党を支援しよう
  - 活動
tags:
  - CoinIMPで政党を支援しよう
  - GitLab

---

{{<tweet 1385447606495715330>}}

こちらのツイートの通りです。

具体的には、以下のURLが変更となっております。

  |以前のURL|変更後のURL|
  |---|---|
  |/index.php[^1]|/index.html|
  |/miner/{政党名の英語略称}.php|/miner/{政党名の英語略称}.html|

ブックマーク等されていた方は、変更をお願いします。

  [^1]: ただし、/でアクセスしていた人が殆どでしょうから、影響はないと思います。

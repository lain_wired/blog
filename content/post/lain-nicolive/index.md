---
title: みんなと委員長で「serial experiments lain」を5時間以上一緒に観よう!
author: Lain
type: post
date: 2020-07-06
slug: lain-nicolive
aliases: /archives/227
image: lain.jpg
categories:
  - 趣味
tags:
  - serial experiments lain

---

※ただの日記です

## serial experiments lain、ニコ生初放送!

7月12日(日)18時45分(アニメ本編は19:00)からニコニコ生放送でserial experiments lain全13話一挙放送が行われます。

ニコ生でlainが放送されるのはこれが史上初です。

さらに同時にVTuber月ノ美兎による実況放送も行われます。

もしものことがない限り、絶対観ます。

## serial experiments lainとは

1998年に放送された鬱アニメです。

放送を楽しんでもらいたいので詳しい内容には触れませんが、高度情報化社会の未来(さらにその暗部まで)を見通した作品です。

私自身大ファンで、ハンドルネームの由来にもしています。

今の時代だからこそ、観る価値があります。

## 月ノ美兎とは

にじさんじ所属のVTuber。

映画研究部所属の学級委員長。

## 視聴URL

  * [「serial experiments lain」全13話一挙放送](https://live2.nicovideo.jp/watch/lv326803291)
  * [月ノ美兎と「serial experiments lain」を見よう](https://live2.nicovideo.jp/watch/lv326785426)

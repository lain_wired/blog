---
title: れいわ新選組への支援打ち切りについて
author: Lain
type: post
date: 2020-06-25
slug: suspend-of-reiwa
aliases: /archives/223
image: reiwa.jpg
categories:
  - 活動
tags:
  - CoinIMPで政党を支援しよう
  - れいわ新選組

---

既に報道されている通り、野党側が宇都宮健児氏を共同候補に立てている2020年東京都知事選に、山本太郎が立候補しました。

  [山本太郎 東京都知事候補特設サイト](https://taro-yamamoto.tokyo/)

よって、れいわ新選組には他党と共闘する気は一切なく、自民党を利するだけだと判断し、支援を打ち切ることにしました。

なお、これに伴い、[CoinIMPで政党を支援しよう](https://donate-party.org/)でれいわ新選組を凍結しました。

---
title: GitLabアカウント作った
author: Lain
type: post
date: 2020-05-21
slug: join-gitlab.com
aliases: /archives/219
image: gitlab.svg
categories:
  - CoinIMPで政党を支援しよう
  - 技術
  - 活動
tags:
  - CoinIMPで政党を支援しよう
  - GitLab

---

GitLabアカウント([@lain_wired](https://gitlab.com/lain_wired))を作りました。

GitHubじゃないのは私がMicrosoftアレルギー患者だからです。

とりあえず、[公式サイト](https://lain-wired.net/)と[CoinIMPで政党を支援しよう](https://donate-party.org/)のソースコードをMITライセンスで公開してあります。

では、よろしくお願いします。

---
title: 立憲民主党と国民民主党の合流について
author: Lain
type: post
date: 2020-09-11
slug: dpfp-joins-cdp
aliases: /archives/241
image: cdp.svg
categories:
  - CoinIMPで政党を支援しよう
  - 活動
tags:
  - CoinIMPで政党を支援しよう
  - 国民民主党
  - 立憲民主党

---

今月10日、立憲民主党と国民民主党の合流新党の代表選と党名投票が行われ、代表は枝野氏、党名は立憲民主党に決定しました。

[【新党代表・党名選挙】代表に枝野幸男衆院議員、党名に立憲民主党を決定](https://cdp-japan.jp/news/20200910_3392)

正式な結党大会は今月15日ですが、これに伴い、[CoinIMPで政党を支援しよう](https://donate-party.org/)で(旧)立憲民主党と国民民主党を(新)立憲民主党に統合しました。

また、 ~~ものはついでということで、~~ 緑の党グリーンズジャパンを追加しました。

---
title: ファシズムの心理学 ~権威に従うということ~ 第4編
author: Lain
type: post
date: 2019-10-04
slug: fascism-psychology-4
aliases: /archives/126
image: VE_301-e1570075040713.jpg
categories:
  - 社会心理学
  - 講義
tags:
  - ナチス
  - ヒトラー
  - ファシズム

---

  * [第1編](/article/fascism-psychology-1/)
  * [第2編](/article/fascism-psychology-2/)
  * [第3編](/article/fascism-psychology-3/)
  * **第4編**
  * [第5編](/article/fascism-psychology-5/)

第4編では、ファシズム体制における人々の心理からは一旦離れて、そもそも何故ファシズムが産まれるのかを考察します。

## ファシズムは民主主義の中で産まれた

前提として、このことを知っておいて下さい。

ナチス･ドイツは銃剣で国民を脅して独裁政権を作り出したのではなく、当時世界で最も民主的と言われたワイマール憲法下の制度を利用する形で、ある意味合法的に独裁政権を樹立したのです。

では、何故当時のドイツ国民はナチ党などに魅力を感じたのか。

### ヒトラーは天才だった

誤解を恐れずに言います。

感情に訴えて大衆を扇動することにおいて、これほど有能な人物を、私は他に思いつきません。

では、彼はどのようにして大衆を扇動したのか。

### ヒトラーは論理ではなく感情に訴えた

そもそも、ファシズムは合理的思考体系を持ちません。

「我が闘争」を一度じっくり読んでみると分かりますが、ヒトラーの言っていることは論理的には無茶苦茶です。

民衆が皆、ちゃんと政治に関心を持って、自分で論理的に考えている社会では、このような政治家及び政党が勢力を伸ばせるはずはありません。

しかし、民衆は往々にして、自分で論理的に考えることを放棄し、感情に訴えるものに走ります。

何故なら、その方が、自分で考える必要が無くて、楽だからです。[^1]

そこで、「何となく良さそう」という政党に飛びついてしまう。

今の日本だってそうでしょう、維新･希望……「何となく良さそう」で一瞬支持を集め、直ぐに消えた政党達です。

ここで、橋下徹の1つのツイートをお見せしましょう。

{{< tweet 200352644252975104 >}}

私は、もし維新が政権を執ったら、日本の民主主義は終わると思っていました。理由は書きません。  
書く必要もないでしょう。

### ナチスは「効果的な宣伝手法」を研究し尽くしている

ヒトラーは演説の天才でしたが、それで全国民を動かすには、先述した様に「何となく良さそう」と思わせることが必要です。

そこで、ナチスは「効果的な宣伝手法」を研究し尽くしました。

具体的には、

  * 話術
  * ラウドスピーカー
  * 飛行機
  * ラジオ

などがあります。

#### 話術

こちらはヒトラーの演説です。  
一度こちらを聞いてみて下さい。(日本語字幕付きです)

{{< nicovideo sm32866239 >}}

すごい。  
ドイツ語なんて分からないのに、聞き入ってしまう。

具体的にはヒトラーは、「ワンフレーズ繰り返し」、「配列」、「修辞法」、「身振り手振り」、「誇張表現」などを多用しています。

**ワンフレーズ繰り返し**

ヒトラーは、「我が闘争」の中で、以下の様なことを述べています。[^2]

  >大衆の受容能力は非常に限られており、理解力は小さいが、そのかわりに忘却力は大きい。この事実からすべて効果的な宣伝は、重点をうんと制限して、そしてこれをスローガンのように利用し、そのことばによって、目的としたものが最後の一人にまで思いうかべることができるように継続的に行われなければならない。

つまり、ヒトラーは単純なスローガンを多用し、よく考えさせずに民衆に合唱させることで支持を集めたのです。  
つまり、政治家が発する言葉を唱える前には、一度立ち止まって考えるべきなのです。

**配列**

序論→陳述→論証→結論という古代ギリシャの弁論術の流れに、過去－現在－未来という時間軸を合わせて演説。  
起承転結分かり易くまとめて、主張を伝えています。  
ただ、分かり易いという理由だけで、信用するのはやめましょう。

**修辞法**

修辞法、特に対比法[^3]を多用しています。  
一つのこと[^4]を引き合いに出して、それを否定し、主張したいものを際立たせるのです。  
普通に伝えるよりもよく伝わりそうでしょう?

**身振り手振り**

これ、話するときに出来ないという人多いんじゃないでしょうか。

**誇張表現**

言うまでもありません。  
Post truthです。

#### ラウドスピーカー

いくら話術が上手くても、それが国民に伝わらなければ何の意味もありません。  
そこで、ナチ党はまだ演説は地声が当たり前だった時代に、演説に最適化されたマイク&スピーカーを開発、さらに会場全体に声が届くようにするためのスピーカーの配置なども研究しました。[^5]

#### 飛行機

当時、国内移動の主要手段は電車でした。  
しかし、ドイツには16もの連邦州があり、その全てを回って演説をしなければなりません。  
その上、ヒトラーは出獄直後から、[^6]その影響力の大きさを懸念した各州政府によって、次々と公開演説禁止命令が出されていました。  
そこで、ヒトラーは最新鋭の乗り物である飛行機を使い、禁令が出ていない地域を飛び回っては、演説を行いました。

#### ラジオ

  ![ナチスが生産させた、国民ラジオVE-301](VE_301-e1570075040713.jpg)

当時、ラジオは一部の金持ちしか持てない、超高級品でした。  
しかし、ヒトラーは自分の演説をドイツ中に届けるためにラジオを利用することにし、政権掌握後、電気メーカーに命令して、安価な[^7]国民ラジオを大量生産させ、国内に普及させました。  
ただ、最終的にはラジオでヒトラーの演説を聞くことが国民の義務とされ、最初の内はまだ良かったものの、戦局の悪化に伴って国民の中にも厭戦感情が蔓延していき、ヒトラーの言葉も空虚な物となるという結果となりました。

### ヒトラーは徹底して民衆の人気取りを行った

いくら演説が上手くても、実績が伴わなければ国民はついて来ません。  
政権を執るまでは「まだ野党だから」と言えますが、与党になっても何も出来なければ、国民から見放されます。  
例え独裁国家でも、国民の大多数から見放されてしまえば、政権運営を続けるのは困難です。
そこで、ナチスは、徹底して民衆の人気取りを行いました。

具体的には高速道路&軍用滑走路を兼ねたアウトバーン[^8]の建設による失業者の救済、  
民意高揚のためのベルリンオリンピック、[^9]
ナショナリズムを煽り、数百万人の犠牲者を出した反ユダヤ主義、  
「富裕層の乗り物であった自動車を庶民の物にする」として開発され、4年積み立て[^10]で販売されたフォルクスワーゲン[^11]などがあります。  
紙面の都合上ここで全てを取り上げることはしませんが、このようにナチスは徹底して民衆の人気取りを行いました。

これはその他のファシズム国家でも同じです。  
例えばムッソリーニのイタリアだって、黒シャツ隊に入ったらあのかっこいい軍服が貰えるという具合で支持を集めました。  
そのようなものに流されては駄目なのです。

## まとめ

何も考えずに「何となく良さそう」と思ってついていくことによってファシズムは生まれます。  
そもそも、ファシズムが生まれる原因も思考停止なのです。  
「何となく良さそう」と思ってついていくと大変な目に遭います。  
立ち止まって考えることが必要なのです。

[次回](/article/fascism-psychology-5/)に続きます。

  [^1]: 但し、楽な事、必ずしも楽しからず。
  
  [^2]: ヒトラー「我が闘争」第一巻
  
  [^3]: 「AではなくB」という表現のこと
  
  [^4]: ヒトラーの場合、ユダヤ人、マルクス主義、ベルサイユ条約など
  
  [^5]: これは、今日でもコンサートなどで応用されています。
  
  [^6]: ヒトラーは一度、ミュンヘン一揆というクーデター未遂事件を起こし、5年間投獄されていました。
  
  [^7]: 写真のVE-301の場合、76ライヒスマルク(現在の日本円では約5万千円)、その後より安価なモデルも出ました。
  
  [^8]: これは、現在でもドイツの高速道路として使われています。
  
  [^9]: 余談ですが、アテネからの聖火リレー、オリンピック記録映画の作成は1936年のベルリンオリンピックから始まりました。また、この際制作された記録映画「オリンピア」は、映画史上に残る傑作と言われています。
  
  [^10]: 990ライヒスマルク(現在の日本円で約74万円弱)、毎週5ライヒスマルクずつ払うことになっていた
  
  [^11]: ただし、戦局の悪化によって軍用車に転用され、実際には国民の手には渡らなかった

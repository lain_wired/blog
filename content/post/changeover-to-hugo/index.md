---
title: 当ブログのシステム変更について
author: Lain
type: post
date: 2021-03-01
slug: changeover-to-hugo
image: hugo.svg
categories:
  - 技術
tags:
  - Hugo
  - GitLab

---

デザインが変わっていることからお察しかもしれませんが、当ブログのシステムを[WordPress](https://ja.wordpress.org/) on 独自サーバー上のLAMP環境から[Hugo](https://gohugo.io/) on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)に変更しました。

レポジトリはこちらです。

https://gitlab.com/lain_wired/blog

ライセンスは

  |適用場所|License|
  |---|---|
  |Hugoプロジェクトのソースコード|GNU GENERAL PUBLIC LICENSE Version 3|
  |ブログ記事|CC BY-NC-SA 4.0|

です。

以前のWordPressブログのときのURLにはリダイレクトを設定してありますが、リンク等を貼ってくださっている方々には新URLへの変更をお願いします。

コメント欄が無くなってしまいましたが、特に積極的に活用してはいなかったので構わないと判断しました。

なお、移行理由は

  * サーバーの負荷軽減
  * サーバーメンテナンスの手間軽減
  * 応答速度向上
  * Markdown記法の利用による執筆効率向上
  * デフォルトで脚注やソースコードが書けて便利なため[^1]
  * Git管理の方が気持ちいいため
  * 静的サイト化によるセキュリティの向上
  * 不正ログインを試みている連中のログを見て嫌になったこと[^2]

などです。

  [^1]: 以前のWordPressブログでは脚注を書くため[Easy Footnotes](https://ja.wordpress.org/plugins/easy-footnotes/)という拡張機能を導入し、ソースコードは[Pastebin](https://pastebin.com/)に投稿した上、埋め込みコードを使っていました。
  
  [^2]: 私も様々なセキュリティ対策を講じて簡単には乗っ取られないようにはしていましたが、毎週平均1000件ほど不正ログイン試行のログが残っており、嫌になりました。

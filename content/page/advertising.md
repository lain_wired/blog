---
title: "広告出稿について"
description: "Anonymous Adsによる広告出稿方法"
date: 2021-2-19
author: "Lain"
slug: "advertising"
---

以下の手順で、私が運営するサイトの広告枠に、広告を出すことができます。

広告からの収入は全て、私の社会活動のために使われます。

## 概要

広告は全て[Anonymous Ads](http://a-ads.com?partner=1226113)というサービスを用いたものです。

費用は仮想通貨[^1]でのみ支払い可能です。

## 出稿方法

広告の右上のアイコンをマウスポイント→Your ad hereをクリックして下さい。 
Anonymous Adsのサイトに飛ばされます。

一日定額で広告を出したいならばDaily budgetを、クリック回数に応じて広告を出したいならばCPM(beta)を選んで下さい。  
次に何語の広告を出したいかをLanguageで選んで下さい。  
その下の予算はお好みで記入してください。  
CPMの方で出稿する場合は希望するクリック回数も設定して下さい。

その下で

  * タイトル*
  * 説明*
  * リンク*
  * 画像

を設定します。(*が付いている物は必須です。)

その後、Anonymous Adsのクーポンコードを持っている場合はI have a coupon codeからそれを入力し、Start campaignをクリックします。

後は画面の指示に従って広告を開始してください。
Anonymous Adsにユーザー登録しなくても出稿できます。

[^1]:
    * Bitcoin
    * Bit Torrent
    * Bitcoin Cash
    * Bitcoin Gold
    * Dash
    * DigiByte
    * Dogecoin
    * Ether Classic
    * Ethereum
    * Komodo
    * Litecoin
    * Neo
    * PIVX
    * Qtum
    * Ravencoin
    * Tron
    * Tether (USDT)
    * Waves
    * Ripple
    * Verge
    * ZCash
    * Horizen
    
    が使用可能です。
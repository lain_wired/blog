---
title: "このサイトについて"
description: "当ブログの目的について"
date: 2021-2-17
author: "Lain"
slug: "about"
---

## 目的

副題にも書いている通り、このサイトでは主に次の4つのジャンルで記事を書きます。

  * 政治
  * 技術
  * 講義
  * 趣味

政治ジャンルは社会活動家としての意見発信の場として書きます。

技術ジャンルはWebデベロッパー、プログラマー、後少しサイバーセキュリティに関して知識がある(と言ってもスクリプトキディですが)人として、運営するサービスの話、便利なツールの紹介などを行います。

講義は、好きな政治的テーマについて、授業のようなものをしようと思っています。

趣味ジャンルは、その名の通りです。

時間があれば聴いて(読んで)ください。

## ライセンス

当サイトは[Hugo](https://gohugo.io/)を用いて作成されております。

当サイトのHugoプロジェクトのライセンスはGNU GENERAL PUBLIC LICENSE Version 3、記事のライセンスはCC BY-NC-SA 4.0です。

このライセンスに従う限り、当サイトのソースコードは誰でも[GitLab](https://gitlab.com/lain_wired/blog)より取得して自由に利用できます。

## ホスティング先

当ブログは[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)にホスティングされています。

## 広告出稿について

私の運営するサイトの広告枠には広告を出稿できます。
方法は[こちら](/advertising/)をご覧ください。
